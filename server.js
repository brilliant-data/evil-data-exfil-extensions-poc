const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors');

const app = new express();

const PORT = 9090;

app.use(cors())

// serve static content
app.use(express.static('public'));

// parse application/json
app.use(bodyParser.json({limit: '50mb'}));

app.post('/dump', (req, res)=>{
  console.log("PAYLOAD: >>>\n", JSON.stringify(req.body), "\n<<<");
  res.send('{"ok": true}');
});



app.listen(PORT, () => console.log(`Evil Leaker App listening on port ${PORT}!`));
