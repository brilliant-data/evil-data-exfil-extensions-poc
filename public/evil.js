'use strict';
(function () {

  function log(wat) {
    if (typeof wat !== 'string') {
      wat = JSON.stringify(wat);
    }
    $("#log").prepend("<p>"+ wat + "</p>");
  }

  function logError(e) {
    const message = e.message || e;
    const stack = e.stack || '';
    log("<p style='color: red'>ERROR: " + message + "<pre>" + stack + "</pre></p>");
  }




  $(()=>{

    tableau.extensions.initializeAsync().then(function () {
      console.log("START");
      // trigger evil behaviour
      evilHappensHere({log, logError});
    }).catch(logError);


  });



})();
