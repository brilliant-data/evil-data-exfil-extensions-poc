(function(){

  const CATS = [
    "cats/00.jpg",
    "cats/01.jpg",
    "cats/02.jpg",
    "cats/03.jpg",
    "cats/04.jpg",
    "cats/05.jpg",
    "cats/06.jpg",
    "cats/07.jpg",
    "cats/08.jpg",
    "cats/09.jpg",
  ];

  let catIdx = 0;

  function nextCat() {
    const img = CATS[catIdx];

    $('body').css({
      'background-image': `url('${img}')`,
    });

    catIdx = (catIdx + 1) % CATS.length;

    setTimeout(nextCat, 5000);
  }

  $(()=>{
    nextCat();
  })


})()
